<?php
/**
 * Headline block
 *
 * @package      wptmpl
 * @author       E-bureauet
 * @since        1.0.0
 * @license      GPL-2.0+
**/

$isTopHeadline = get_field('istopheadline');
$title = get_field('title');
$subtitle = get_field('subtitle');
?>
<header class="blk blk--headline">
  <div class="blk__inner">
    <?php if ($isTopHeadline) : ?>
    <div class="headline">
      <h1 class="headline__primary"><?php echo $title; ?></h1>
      <?php if ($subtitle) :?>
      <h2 class="headline__secondary"><?php echo get_field('subtitle'); ?></h2>
      <?php endif; ?>
    </div>
    <?php else : ?>
    <div class="headline">
      <h2 class="headline__primary"><?php echo $title; ?></h2>
      <?php if ($subtitle) :?>
      <h3 class="headline__secondary"><?php echo get_field('subtitle'); ?></h3>
      <?php endif; ?>
    </h2>
    <?php endif; ?>
  </div>
</header>