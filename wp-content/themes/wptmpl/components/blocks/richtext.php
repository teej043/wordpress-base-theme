<?php
/**
 * Richtext block
 *
 * @package      wptmpl
 * @author       E-bureauet
 * @since        1.0.0
 * @license      GPL-2.0+
**/

$slideshow = get_field('content');

?>
<section class="blk blk--richtext">
  <div class="blk__inner">
    <div class="richtext">
      <div class="texts">
        <?php echo get_field('content'); ?>
      </div>
    </div>
  </div>
</section>