<?php
/**
 * Accordion block
 *
 * @package      wptmpl
 * @author       E-bureauet
 * @since        1.0.0
 * @license      GPL-2.0+
**/

$items = get_field('items');
?>

<section class="blk blk--accordion">
  <div class="blk__inner">
    <div class="accordion">
    <?php 
      foreach($items as $item):
      $title = $item['heading'];
      $content = $item['content'];
      $isOpened = $item['opened'];
    ?>
      <div class="accordion__item <?php echo $isOpened ? '' : 'collapsed'; ?>">
        <header>
          <h3>
          <?php echo $title; ?>
          </h3>
        </header>
        <article>
          <div class="texts">
          <?php echo $content; ?>
          </div>
        </article>
      </div>
    <?php endforeach; ?>
    </div>
  </div>
</section>

