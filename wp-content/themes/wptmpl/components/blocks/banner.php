<?php
/**
 * Herobanner block
 *
 * @package      wptmpl
 * @author       E-bureauet
 * @since        1.0.0
 * @license      GPL-2.0+
**/



$bannerImage = get_field('image');
$bannerContent = get_field('content');

//var_dump($slideshow);
//var_dump($customSlides);

?>
<section class="blk blk--banner" role="banner">
  <div class="blk__inner">
    <div class="banner">
      <picture class="banner__image">
        <source
          data-srcset="<?php echo $bannerImage['sizes']['hero-mobile']; ?>"
          media="(max-width: 500px)" />
        <source
          data-srcset="<?php echo $bannerImage['sizes']['hero']; ?>"
          media="(min-width: 501px)" />
        <img class="lazyload" src="" data-src="<?php echo $bannerImage['sizes']['hero']; ?>" alt="<?php echo($bannerImage['alt']) ?>">
      </picture>
      <?php if ($bannerContent) : ?>
      <div class="banner__content">
        <div class="texts">
          <?php echo $bannerContent; ?>
        </div>
      </div>
      <?php endif; ?>  
    </div>
  </div>
</section>