<?php
/**
 * Tiles block
 *
 * @package      wptmpl
 * @author       E-bureauet
 * @since        1.0.0
 * @license      GPL-2.0+
**/

$tiles = get_field('items');

?>
<section class="blk blk--tiles">
  <div class="blk__inner">
    <div class="tiles">
    <?php 
      foreach($tiles as $key => $tile):
        $image = $tile['image'];
        $content = $tile['content'];
    ?>
      <div class="tiles__item tile">
      <?php if ($image) : ?>
        <picture class="tile__image">
          <source
            data-srcset="<?php echo $image['sizes']['tile-tall']; ?>"
            media="(max-width: 500px)" />
          <source
            data-srcset="<?php echo $image['sizes']['tile-square']; ?>"
            media="(min-width: 501px)" />
          <img class="lazyload" src="" data-src="<?php echo $image['sizes']['tile-square']; ?>" alt="<?php echo($image['alt']) ?>">
        </picture>
      <?php endif; ?>
      <?php if ($content): ?>
        <div class="tile__content">
          <div class="texts">
            <?php echo $content; ?>
          </div>
        </div>
      <?php endif; ?>
      </div>
    <?php endforeach; ?>
    </div>
  </div>
</section>