<?php
/**
 * Herobanner block
 *
 * @package      wptmpl
 * @author       E-bureauet
 * @since        1.0.0
 * @license      GPL-2.0+
**/



$slideshow = get_field('slideshow');
$customSlides = get_field('custom_slides');

//var_dump($slideshow);
//var_dump($customSlides);

?>
<section class="blk blk--herobanner" role="banner">
  <div class="blk__inner">
    <div class="herobanner">
      <div class="slideshow herobanner__slideshow">
        <div class="slides">
          <?php 
          foreach($customSlides as $p):
            $slideImg = $p['img'];
            $slideContent = $p['content'];
          ?>
          <div class="slide">
            <picture class="slide__image">
              <img src="<?php echo $slideImg['sizes']['hero']; ?>" alt="<?php echo($slideImg['alt']) ?>">
            </picture>
            <?php if ($slideContent) : ?>
            <div class="slide__content">
              <div class="texts">
                <?php echo $slideContent; ?>
              </div>
            </div>
            <?php endif; ?>  

          </div>

          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</section>