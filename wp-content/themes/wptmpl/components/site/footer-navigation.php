<nav>
  <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'wptmpl' ); ?></button>
  <?php
  wp_nav_menu( array(
    'theme_location' => 'footer',
    //'menu_id'        => 'main-menu',
  ) );
  ?>
</nav>