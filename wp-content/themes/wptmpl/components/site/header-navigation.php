<?php 
	// Top Nav Walker class
	class top_nav_menu extends Walker_Nav_Menu {
		// add classes to ul sub-menus
    function start_lvl( &$output, $depth = 0, $args = array() ) {
			// depth dependent classes
			$indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
			$display_depth = ( $depth + 1);
			$classes = array(
					'sub-menu',
					'menu-depth-' . $display_depth
			);
			$class_names = implode( ' ', $classes );

			if ($display_depth == 1) {
				$output .= "\n" . $indent . '<div class="sub-menu-wrapper"><div class="sub-menu-filler"></div><ul class="' . $class_names . '">' . "\n";
			} else {
				$output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
			}
		}
	}
?>

<nav id="site-navigation" class="main-navigation" role="navigation">
  <?php wp_nav_menu( array( 
    'theme_location' => 'menu-1', 
    'menu_id' => 'top-menu',
    'link_before' => '<span class="l">',
    'link_after' => '</span><div class="arrow"></div>',
    'walker' => new top_nav_menu
  ) ); ?>

  <?php if ($isLocalized) :  ?>
  <div class="menu-secondary-container">
    <ul>
      <li class="menu-item menu-item--language">
        <a href="#language-selector">
          <i class="icon-globe"></i><span class="l"><?php _e('Select language','coro'); ?></span>
        </a>
      </li>
    </ul>
  </div>
  <?php endif; ?>
</nav>