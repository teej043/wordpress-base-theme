<div class="site-branding">
  <?php 
    $brandImage = get_field('header_branding','option');
    if ($brandImage) :
  ?> 
  <figure class="logo">
    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
      <img src="<?php echo $brandImage['sizes']['large']; ?>" alt="<?php echo $brandImage['alt']; ?>">
    </a>
  </figure>
  <?php endif; ?>

  <!-- DISABLE DISPLAY OF TITLE AND DESCRIPTIOn
  <?php
  if ( is_front_page() && is_home() ) :
    ?>
    <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
    <?php
  else :
    ?>
    <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
    <?php
  endif;
  $wptmpl_description = get_bloginfo( 'description', 'display' );
  if ( $wptmpl_description || is_customize_preview() ) :
    ?>
    <p class="site-description"><?php echo $wptmpl_description; /* WPCS: xss ok. */ ?></p>
  <?php endif; ?>
  -->
  
</div><!-- .site-branding -->