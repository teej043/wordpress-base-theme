<?php 
/**
 * GET Gutenberg wptmpl Blocks
 */
require get_template_directory() . '/inc/blocks.php';


/**
 *  Add ACF admin styles
 */
function my_acf_admin_head() {
	?>
	<style type="text/css">

    .acf-flexible-content .layout{
      border-radius: 10px;
      overflow: hidden;
    }

    .acf-flexible-content .layout .acf-fc-layout-controlls .acf-icon.-collapse:hover {
        color: #fff;
        background-color: silver;
    }
    
    .acf-flexible-content .layout .acf-fc-layout-handle {
        /*background-color: #00B8E4;*/
        background-color: #32373c;
        color: #eee;
    }

    .acf-repeater.-row > table > tbody > tr > td,
    .acf-repeater.-block > table > tbody > tr > td {
        border-top: 2px solid #32373c;
    }

    .acf-repeater .acf-row-handle {
        vertical-align: top !important;
        padding-top: 16px;
    }

    .acf-repeater .acf-row-handle span {
        font-size: 20px;
        font-weight: bold;
        color: #32373c;
    }

    .imageUpload img {
        width: 75px;
    }

    .acf-repeater .acf-row-handle .acf-icon.-minus {
        top: 30px;
    }

</style>
<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');


/**
 * ACF Hide Custom Fields Menu from production sites
 */
function acf_hide_acf_admin() {
	// get the current site url
	$site_url = get_bloginfo( 'url' );
	
	// an array of protected site urls  
	$protected_urls = array(
		'https://wptmpl.com',
	);

	// check if the current site url is in the protected urls array  
	if ( in_array( $site_url, $protected_urls ) ) {
		// hide the acf menu item 
		return false;
	} else {
		// show the acf menu item 
		return true;
	}
}
add_filter('acf/settings/show_admin', 'acf_hide_acf_admin');



/**
 * ACF Faster load time snippet
 */
add_filter('acf/settings/remove_wp_meta_box', '__return_true');



/**
 * ACF Allow Fields Updates Json modifications
 */

add_filter('acf/settings/load_json', function($paths) {
  $paths = array();

  if(is_child_theme())
  {
    $paths[] = get_stylesheet_directory() . '/acf-json';
  }
  $paths[] = get_template_directory() . '/acf-json';

  return $paths;
});



/**
 * FOR GLOBAL OPTION ACF FIELDS
 */

 /**
 *  Add ACF options page
 */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> __('Theme Options','wptmpl'),
		'menu_title'	=> __('Theme Options','wptmpl'),
		'menu_slug' 	=> 'theme-options',
    'capability'	=> 'edit_posts',
    'icon_url' => 'dashicons-admin-appearance',
		'redirect'		=> false
	));
}

 /**
 *  Add ACF global site options page
 */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> __('Global Options','wptmpl'),
		'menu_title'	=> __('Global Options','wptmpl'),
		'menu_slug' 	=> 'global-options',
		'capability'	=> 'install_themes',
		'redirect'		=> false
	));
}

?>
