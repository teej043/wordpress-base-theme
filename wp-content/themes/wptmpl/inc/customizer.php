<?php
/**
 * wptmpl Theme Customizer
 *
 * @package wptmpl
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function wptmpl_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

  
	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'wptmpl_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'wptmpl_customize_partial_blogdescription',
		) );
  }
  
  // Remove unwanted customizer settings
  $wp_customize->remove_control('header_textcolor');
  $wp_customize->remove_control('header_image');
  $wp_customize->remove_control('custom_logo');
  
  $wp_customize->remove_section( 'custom_css' );  
  $wp_customize->remove_section( 'colors' );
  $wp_customize->remove_section( 'background_image' );
  
}
add_action( 'customize_register', 'wptmpl_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function wptmpl_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function wptmpl_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function wptmpl_customize_preview_js() {
	wp_enqueue_script( 'wptmpl-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'wptmpl_customize_preview_js' );


/**
 * Remove Customize Menu
 */
function coro_before_admin_bar_render()
{
  global $wp_admin_bar;
  $wp_admin_bar->remove_menu('customize');
}
add_action( 'wp_before_admin_bar_render', 'coro_before_admin_bar_render' ); 
