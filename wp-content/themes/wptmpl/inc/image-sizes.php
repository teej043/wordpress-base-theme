<?php
/*
  * Enable support for Post Thumbnails on posts and pages.
  *
  * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
  */
add_theme_support( 'post-thumbnails' );

add_image_size( 'hero', 1920, 600, true );
add_image_size( 'hero-tablet', 1920, 625, true );
add_image_size( 'hero-mobile', 824, 824, true );

add_image_size( 'tile-square', 600, 600, true );
add_image_size( 'tile-square-2x', 1200, 1200, true );
add_image_size( 'tile-wide', 1280, 720, true );
add_image_size( 'tile-tall', 720, 1280, true );

// Set the Jpeg Quality
add_filter('jpeg_quality', function($arg){return 95;});

?>