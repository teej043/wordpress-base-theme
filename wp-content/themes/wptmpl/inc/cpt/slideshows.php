<?php
// Register Custom Post Type
function cpt_slideshow() {

	$labels = array(
		'name'                  => _x( 'Slideshows', 'Post Type General Name', 'wptmpl' ),
		'singular_name'         => _x( 'Slideshow', 'Post Type Singular Name', 'wptmpl' ),
		'menu_name'             => __( 'Slideshows', 'wptmpl' ),
		'name_admin_bar'        => __( 'Slideshows', 'wptmpl' ),
		'archives'              => __( 'Slideshow Archives', 'wptmpl' ),
		'attributes'            => __( 'Slideshow Attributes', 'wptmpl' ),
		'parent_item_colon'     => __( 'Parent Slideshow:', 'wptmpl' ),
		'all_items'             => __( 'All Slideshows', 'wptmpl' ),
		'add_new_item'          => __( 'Add New Slideshow', 'wptmpl' ),
		'add_new'               => __( 'Add New', 'wptmpl' ),
		'new_item'              => __( 'New Slideshow', 'wptmpl' ),
		'edit_item'             => __( 'Edit Slideshow', 'wptmpl' ),
		'update_item'           => __( 'Update Slideshow', 'wptmpl' ),
		'view_item'             => __( 'View Slideshow', 'wptmpl' ),
		'view_items'            => __( 'View Slideshows', 'wptmpl' ),
		'search_items'          => __( 'Search Slideshow', 'wptmpl' ),
		'not_found'             => __( 'Not found', 'wptmpl' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'wptmpl' ),
		'featured_image'        => __( 'Featured Image', 'wptmpl' ),
		'set_featured_image'    => __( 'Set featured image', 'wptmpl' ),
		'remove_featured_image' => __( 'Remove featured image', 'wptmpl' ),
		'use_featured_image'    => __( 'Use as featured image', 'wptmpl' ),
		'insert_into_item'      => __( 'Insert into Slideshow', 'wptmpl' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Slideshow', 'wptmpl' ),
		'items_list'            => __( 'Slideshow list', 'wptmpl' ),
		'items_list_navigation' => __( 'Item Slideshow navigation', 'wptmpl' ),
		'filter_items_list'     => __( 'Filter Slideshows list', 'wptmpl' ),
	);
	$args = array(
		'label'                 => __( 'Slideshow', 'wptmpl' ),
		'description'           => __( 'A collection of slideshow materials', 'wptmpl' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-slides',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'slideshow', $args );

}
add_action( 'init', 'cpt_slideshow', 0 );

?>