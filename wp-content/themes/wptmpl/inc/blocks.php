<?php

/*
add_filter( 'block_categories', 'wptmpl_block_module_category', 10, 2 );
function wptmpl_block_module_category( $categories, $post ) {
  if ( $post->post_type !== 'post' ) {
      return $categories;
  }
  return array_merge(
      $categories,
      array(
          array(
              'slug' => 'wptmpl-modules',
              'title' => __( 'Wordpress Theme Template Modules', 'wptmpl' ),
              'icon'  => null,
          ),
      )
  );
}
*/


/**
 * Register Blocks
 *
 */
function wptmpl_register_blocks() {
	if( ! function_exists('acf_register_block') )
    return;
    
	acf_register_block( array(
		'name'			=> 'herobanner',
		'title'			=> __( 'Herobanner', 'wptmpl' ),
		'render_template'	=> 'components/blocks/herobanner.php',
		'category'		=> 'common',
		'icon'			=> 'images-alt',
    //'mode'			=> 'edit',
    'mode'			=> 'preview',
    'post_types' => array('post', 'page'),
    'keywords'		=> array( 'herobanner', 'banner', 'topbanner', 'sliders', 'slideshows' ),
    'supports' => array(
      'align' => false,
      'multiple' => false,
      'reusable' => true
      //'mode' => false
    ),
    'enqueue_assets' => function(){
      wp_enqueue_script( 'global', get_template_directory_uri() . '/assets/js/global.min.js', array('jquery'), '20200106', true );
      wp_enqueue_script( 'lazysizes', get_template_directory_uri() . '/assets/js/vendors/lazysizes.min.js', array('jquery'), '20200106', true );
      wp_enqueue_script( 'slick', get_template_directory_uri() . '/assets/js/vendors/slick.min.js', array('jquery'), '20200106', true );
      wp_enqueue_script( 'blk--herobanner', get_template_directory_uri() . '/assets/js/blocks/herobanner.min.js', array('slick'), '20200106', true );
    }
  ));

  acf_register_block( array(
		'name'			=> 'richtext',
		'title'			=> __( 'Richtext', 'wptmpl' ),
		'render_template'	=> 'components/blocks/richtext.php',
		'category'		=> 'common',
		'icon'			=> 'editor-justify',
    'mode'			=> 'preview',
    'post_types' => array( 'post', 'page' ),
    'keywords'		=> array( 'richtext', 'texts', 'rte' ),
    'supports' => array(
      'align' => false,
    )
  ));

  acf_register_block( array(
		'name'			=> 'tiles',
		'title'			=> __( 'Tiles', 'wptmpl' ),
		'render_template'	=> 'components/blocks/tiles.php',
		'category'		=> 'common',
		'icon'			=> 'screenoptions',
    'mode'			=> 'preview',
    'post_types' => array( 'post', 'page' ),
    'keywords'		=> array( 'tiles' ),
    'supports' => array(
      'align' => false,
      'reusable' => true
    ),
    'enqueue_assets' => function(){
      wp_enqueue_script( 'global', get_template_directory_uri() . '/assets/js/global.min.js', array('jquery'), '20200106', true );
      wp_enqueue_script( 'lazysizes', get_template_directory_uri() . '/assets/js/vendors/lazysizes.min.js', array('jquery'), '20200106', true );
    }
  ));

  acf_register_block( array(
		'name'			=> 'accordion',
		'title'			=> __( 'Accordion', 'wptmpl' ),
		'render_template'	=> 'components/blocks/accordion.php',
		'category'		=> 'common',
		'icon'			=> 'sort',
    'mode'			=> 'preview',
    'post_types' => array( 'post', 'page' ),
    'keywords'		=> array( 'accordion' ),
    'supports' => array(
      'align' => false,
      'reusable' => true
    ),
    'enqueue_assets' => function(){
      wp_enqueue_script( 'blk--accordion', get_template_directory_uri() . '/assets/js/blocks/accordion.min.js', array('jquery'), '20200106', true );
    }
  ));

  acf_register_block( array(
		'name'			=> 'banner',
		'title'			=> __( 'Banner', 'wptmpl' ),
		'render_template'	=> 'components/blocks/banner.php',
		'category'		=> 'common',
		'icon'			=> 'flag',
    'mode'			=> 'preview',
    'post_types' => array( 'post', 'page' ),
    'keywords'		=> array( 'banner' ),
    'supports' => array(
      'align' => false,
    ),
    'enqueue_assets' => function(){
      wp_enqueue_script( 'global', get_template_directory_uri() . '/assets/js/global.min.js', array('jquery'), '20200106', true );
      wp_enqueue_script( 'lazysizes', get_template_directory_uri() . '/assets/js/vendors/lazysizes.min.js', array('jquery'), '20200106', true );
    }
  ));

  acf_register_block( array(
		'name'			=> 'headline',
		'title'			=> __( 'Headline', 'wptmpl' ),
		'render_template'	=> 'components/blocks/headline.php',
		'category'		=> 'common',
		'icon'			=> 'format-quote',
    'mode'			=> 'preview',
    'post_types' => array( 'post', 'page' ),
    'keywords'		=> array( 'headline', 'heading', 'header' ),
    'supports' => array(
      'align' => false,
    )
  ));

}
add_action('acf/init', 'wptmpl_register_blocks' );



add_filter( 'allowed_block_types', 'wptmpl_allowed_block_types' );
function wptmpl_allowed_block_types( $allowed_blocks ) {
 
	return array(
		//'core/image',
    'acf/herobanner',
    'acf/richtext',
    'acf/tiles',
    'acf/banner',
    'acf/accordion',
    'acf/headline'
	);
 
}





?>