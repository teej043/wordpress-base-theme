( function( $ ) {
  var init_banner = function(){
    $('.blk--herobanner').each(function(){
      var $blk = $(this);
      var $slider = $blk.find('.slides');
  
      var conf = {
        arrows: false,
        adaptiveHeight: false,
        slidesToShow: 1,
        infinite: false,
        dots: false,
        fade: false,
        adaptiveHeight: true
      };

      $slider.on('init', function(event, slick){
        setTimeout(function(){
          $slider.slick('setPosition');
        }, 1);
      });
  
      $slider.slick(conf);
  
      console.log('wah');
    });
  }
  
  $(document).ready(function(){
    init_banner();
  });

  // Initialize dynamic block preview (editor).
  if (window.acf) {
    window.acf.addAction('render_block_preview/type=herobanner', init_banner);
  }
  /*
  if (window.acf) {
    setTimeout(
      window.acf.addAction('render_block_preview/type=herobanner', init_banner),
      800)
    
  }
  */

  

} )( jQuery );