( function( $ ) {

  var init_accordion = function(){
    $('.blk--accordion').each(function(){

      var $blk = $(this);
      var $accordions = $blk.find('.accordion__item');
  
      var conf = {};
      $accordions.each(function(){
        var $e = $(this);
        var $title = $e.find('header');
        var $content = $e.find('article');
        var $texts = $content.find('.texts');
        var ht = $content.outerHeight();

        console.log(ht);

        $title.on('click', function(e){
          $e.toggleClass('collapsed');
          ht = $content.outerHeight();
          console.log(ht);
        });

      });

    });

    console.log('wah');
  }
  
  $(document).ready(function(){
    init_accordion();
  });

  // Initialize dynamic block preview (editor).
  if (window.acf) {
    window.acf.addAction('render_block_preview/type=accordion', init_accordion);
  }
  

} )( jQuery );