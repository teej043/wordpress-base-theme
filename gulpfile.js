var gulp = require('gulp'),
    sass = require('gulp-sass'),
    path = require('path'),
    merge = require('merge-stream'),
    rename = require('gulp-rename'),
    watch = require('gulp-watch'),
    sourcemaps = require('gulp-sourcemaps'),
    del = require('del'),
    concat = require('gulp-concat'),
    runSequence = require('run-sequence'),
    cleanCSS = require('gulp-clean-css'),
    path = require('path'),
    iconfont = require('gulp-iconfont'),
    iconfontcss = require('gulp-iconfont-css'),
    wait = require('gulp-wait'),
    merge = require('merge-stream'),
    argv = require('yargs').argv,
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    pipeline = require('readable-stream').pipeline;


// DEV MODE VARIABLE
var isProduction = (argv.production === undefined) ? false : true;


// SVG ICON SPRITING
var svgSprite = require('gulp-svg-sprite');
gulp.task('makeicons', function(callback) {
  runSequence('iconfont', 'sass', 'html');
});

// LIST OF THEMES TO BUILD
var themes = [
  './wp-content/themes/wptmpl'
];

// SASS
gulp.task('sass', function(){
  var tasks = themes.map(function(element){
    console.log(element);
    return gulp.src([
      element + '/src/sass/style.scss', 
      element + '/src/sass/blocks.scss', 
      element + '/src/sass/admin.scss', 
      element + '/src/sass/theme.scss', 
      element + '/src/sass/rtl.scss'
    ], 
    {
      base: element + '/src/sass'
    })
      .pipe( wait(500) )
      .pipe( sourcemaps.init() )
      .pipe( sass().on('error', sass.logError))
      .pipe( sourcemaps.write() )
      .pipe(gulpif(isProduction, cleanCSS()))
      .pipe(gulp.dest(element + '/'));
  });
  return merge(tasks);
});


// ICON FONT
var fontName = 'Icons';
gulp.task('iconfont', function(){
  var tasks = themes.map(function(element){
    return gulp.src(element + '/src/svg/*.svg', {base: element + '/src/svg'})
      .pipe(iconfontcss({
        fontName: fontName,
        path: './node_modules/gulp-iconfont-css/templates/_icons.scss',
        targetPath: '../../../src/sass/shared/_icons.scss',
        fontPath: 'assets/fonts/icons/'
      }))
      .pipe(iconfont({
        fontName: fontName,
        fixedWidth: true,
        centerHorizontally: true,
        normalize: true,
        fontHeight:1000,
        descent: 0
      }))
      .pipe(gulp.dest( element + '/assets/fonts/icons/'));
  });
  return merge(tasks);
});


// JS MINIFICATION
gulp.task('js', function () {
  var tasks = themes.map(function(element){
    console.log(element);
    return pipeline(
      gulp.src([element + '/src/js/**/*.js'], {base: element + '/src/js'}),
      gulpif(isProduction, uglify()),
      rename({
        "suffix" : ".min"
      }),
      gulp.dest(element + '/assets/js/')
    );
  });
  return merge(tasks);
});




// WATCH
gulp.task('default', ['sass','js'], function () {
  gulp.watch("wp-content/themes/**/*.scss", ['sass']);
  gulp.watch("wp-content/themes/**/src/**/*.js", ['js']);
});


// BUILD
gulp.task('build', function() {
  isProduction = true;
  runSequence('sass', 'js');
});