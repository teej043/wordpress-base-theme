# Wordpress Base Theme Template

It is a base template to be used as basis for when developing new themes for new wordpress site.

You can safely put this on any wordpress instance as this does not have its own wordpress core, it only has the required files on the 'wp-content' folder.

What is in it:

* Essential plugins that are required for the theme to work.
* The theme folder which needs to be renamed to something (more about this below).
* Basic Gutenberg blocks.
* Minimalistic styles.

Features:

* Based on [_underscore](https://underscores.me/) starter theme by Automattic.
* Build system for creating optimized/minified assets like css, js and svg icons.
* WPEngine ready.
* A just right amount of lean, well-commented, modern, HTML5 templates.
* A helpful 404 template.
* A custom header implementation in `inc/custom-header.php` just add the code snippet found in the comments of `inc/custom-header.php` to your `header.php` template.
* Custom template tags in `inc/template-tags.php` that keep your templates clean and neat and prevent code duplication.
* Some small tweaks in `inc/template-functions.php` that can improve your theming experience.
* A script at `js/navigation.js` that makes your menu a toggled dropdown on small screens (like your phone), ready for CSS artistry. It's enqueued in `functions.php`.
* 2 sample CSS layouts in `layouts/` for a sidebar on either side of your content.
Note: `.no-sidebar` styles are not automatically loaded.
* Smartly organized starter CSS in `style.css` that will help you to quickly get your design off the ground.
* Full support for `WooCommerce plugin` integration with hooks in `inc/woocommerce.php`, styling override woocommerce.css with product gallery features (zoom, swipe, lightbox) enabled.

## Getting Started

Below are the primary requirements to get you started.

### Prerequisites

* Code Editor like [Visual Code](https://code.visualstudio.com/).
* GIT for source control and deployment for WPengine [download here](https://git-scm.com/downloads)
* Make sure you have NodeJS installed. 
  * [Gulp](https://gulpjs.com/) is required.
  * Node-sass requires Visual Studio build tools so you might need to install those. [more info here](https://github.com/nodejs/node-gyp#installation).   
* Make sure you have a PHP and Mysql webserver like WAMP or XAMMP if you are working locally.
* Make sure you have an existing Wordpress instance ready.

### Installing

You actually do not need to clone this repository, just download as a zip file and extract to the Wordpress instance (root folder where 'wp-content' resides).

The build system requires node modules so once you have the files extracted you need to go to the root directory of Wordpress instance and then open up terminal to enter this command:

```
npm install
```

The command above will install all the required node modules in order for the build system to function. The whole process might take a while to finish so be patient.

Once the node modules installation is done you are now ready to start working by entering this command:

```
gulp
```

### Renaming the Theme

You might want to rename the theme "wptmpl" to something else before you start developing, on this example let's rename it to "my-new-theme".

First, rename the theme folder `'wptmpl'` to `'my-new-theme'`. 

And then you'll need to do a five-step find and replace on the name in all the sub folders and files:

1. Search for `'wptmpl'` (inside single quotations) to capture the text domain.
2. Search for `wptmpl_` to capture all the function names.
3. Search for `Text Domain: wptmpl` in `style.css`.
4. Search for <code>&nbsp;_s</code> (with a space before it) to capture DocBlocks.
5. Search for `wptmpl-` to capture prefixed handles.

OR

1. Search for: `'wptmpl'` and replace with: `'my-new-theme'`.
2. Search for: `wptmpl_` and replace with: `my_new_theme_`.
3. Search for: `Text Domain: wptmpl` and replace with: `Text Domain: my-new-theme` in `style.css`.
4. Search for: <code>&nbsp;wptmpl</code> and replace with: <code>&nbsp;my-new-theme</code>.
5. Search for: `wptmpl-` and replace with: `my-new-theme-`.

Then, update the stylesheet header in style.css, the links in footer.php with your own information and rename wptmpl.pot from languages folder to use the theme's slug.

That's it, you can now start developing this new theme.

### Source Control

Note: Ignore this section if you are already using WPEngine which already uses GIT.

You need to source control your local development by initiating git and having github or bitbucket as your remote repository:

So first go to either github or bitbucker and create your repository.

Once you have the git repository https url then do this:

```
git init
```

and then stage the new files

```
git add --all
```

Add the remote repository

```
git remote add origin https://teej043@bitbucket.org/teej043/stest.git
```

and then push your files to the remote repository.

```
git push -u origin master
```

## Deployment

### WPEngine

On WPEngine you need to be added as developer, or else you won't be able to push via git. An SSH public key might be needed to be generated on your machine and send it to the WPEngine account maintainer to add you as developer.

[Here is a tutorial](https://wpengine.com/git/) on how to connect to WPEngine Git Push

### Others

If your host do not support GIT or Push Deployments then you just simply need to upload the 'wp-content' folder to the file server of your hosting.


## Authors

* **Teejay De Guzman** - *Initial work* - [teej043](https://github.com/teej043)

